### Intro

Реализован декодер PNG изображений, который соответствует стандартной спецификации и преобразует PNG-изображение в
структуру `Image`, которая хранит матрицу пикселей в формате RGB

Точка входа — это функция `Image ReadPng(std::string_view filename)`

### Libraries

1. Libdeflate: https://github.com/ebiggers/libdeflate for deflate
2. Boost for CRC calculation